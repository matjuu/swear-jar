﻿using System;

namespace swear_jar.Domain.Contracts
{
    public abstract class Event : IEvent
    {
        public Guid CorrelationId { get; set; }

        public Guid EventId { get; set; }
        public DateTime EventDate { get; set; }

        protected Event()
        {
            EventId = Guid.NewGuid();
            EventDate = DateTime.UtcNow;
        }
    }
}

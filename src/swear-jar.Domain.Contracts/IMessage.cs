using System;

namespace swear_jar.Domain.Contracts
{
    public interface IMessage
    {
        Guid CorrelationId { get; set; }
    }
}
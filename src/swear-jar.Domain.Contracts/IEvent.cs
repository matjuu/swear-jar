﻿using System;

namespace swear_jar.Domain.Contracts
{
    public interface IEvent : IMessage
    {
        Guid EventId { get; set; }
        DateTime EventDate { get; set; }
    }
}

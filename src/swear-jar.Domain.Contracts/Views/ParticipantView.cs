﻿using System;

namespace swear_jar.Domain.Contracts.Views
{
    public class ParticipantView
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Debt { get; set; }
    }
}

﻿using System;

namespace swear_jar.Domain.Contracts.Views
{
    public class JarView
    {
        public Guid Id { get; set; }
        public int Money { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace swear_jar.Domain.Contracts.Views
{
    public class GroupView
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ParticipantView> Participants { get; set; }
        public JarView Jar { get; set; }
    }
}

﻿using System;

namespace swear_jar.Domain.Contracts.Events
{
    public class DroppedIntoJar : Event
    {
        public Guid Id { get; set; }
        public Guid ParticipantId { get; set; }
        public int Amount { get; set; }
    }
}
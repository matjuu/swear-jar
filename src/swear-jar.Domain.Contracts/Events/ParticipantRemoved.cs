﻿using System;

namespace swear_jar.Domain.Contracts.Events
{
    public class ParticipantRemoved : Event
    {
        public Guid Id { get; set; }
        public Guid ParticipantId { get; set; }
    }
}
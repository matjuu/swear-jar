﻿using System;

namespace swear_jar.Domain.Contracts.Events
{
    public class ParticipantAdded : Event
    {
        public Guid Id { get; set; }
        public Guid ParticipantId { get; set; }
        public string Name { get; set; }
    }
}

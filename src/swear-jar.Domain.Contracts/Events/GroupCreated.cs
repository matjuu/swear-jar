﻿using System;

namespace swear_jar.Domain.Contracts.Events
{
    public class GroupCreated : Event
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
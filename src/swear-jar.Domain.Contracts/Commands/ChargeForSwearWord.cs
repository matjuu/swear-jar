namespace swear_jar.Domain.Contracts.Commands
{
    public class ChargeForSwearWord : MoneyTransactionCommand
    {
        public ChargeForSwearWord() : base(5)
        {
        }
    }
}
﻿using System;

namespace swear_jar.Domain.Contracts.Commands
{
    public class RemoveParticipant : IMessage
    {
        public Guid CorrelationId { get; set; }
        public Guid GroupId { get; set; }
        public Guid ParticipantId { get; set; }
    }
}

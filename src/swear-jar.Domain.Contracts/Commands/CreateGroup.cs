﻿using System;

namespace swear_jar.Domain.Contracts.Commands
{
    public class CreateGroup : IMessage
    {
        public Guid CorrelationId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

using System;

namespace swear_jar.Domain.Contracts.Commands
{
    public abstract class MoneyTransactionCommand : IMessage
    {
        public Guid CorrelationId { get; set; }
        public int Amount { get; private set; }
        public Guid GroupId { get; set; }
        public Guid ParticipantId { get; set; }

        protected MoneyTransactionCommand(int amount)
        {
            Amount = amount;
        }
    }
}
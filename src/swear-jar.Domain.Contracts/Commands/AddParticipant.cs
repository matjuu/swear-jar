﻿using System;

namespace swear_jar.Domain.Contracts.Commands
{
    public class AddParticipant : IMessage
    {
        public Guid CorrelationId { get; set; }
        public Guid GroupId { get; set; }
        public string Name { get; set; }
    }
}

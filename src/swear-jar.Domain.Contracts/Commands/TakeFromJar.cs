﻿namespace swear_jar.Domain.Contracts.Commands
{
    public class TakeFromJar : MoneyTransactionCommand
    {
        public TakeFromJar(int amount) : base(amount)
        {
        }
    }
}
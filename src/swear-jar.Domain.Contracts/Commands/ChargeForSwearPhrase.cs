﻿namespace swear_jar.Domain.Contracts.Commands
{
    public class ChargeForSwearPhrase : MoneyTransactionCommand
    {
        public ChargeForSwearPhrase() : base(10)
        {
        }
    }
}
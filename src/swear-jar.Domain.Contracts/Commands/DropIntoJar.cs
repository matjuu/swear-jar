﻿namespace swear_jar.Domain.Contracts.Commands
{
    public class DropIntoJar : MoneyTransactionCommand
    {
        public DropIntoJar(int amount) : base(amount)
        {
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using swear_jar.Domain.Contracts.Events;
using swear_jar.Domain.Contracts.Views;

namespace swear_jar.Projections
{
    public class GroupProjection : Projection<GroupView>
    {
        public GroupView ApplyEvent(GroupView state, GroupCreated e)
        {
            state.Id = e.Id;
            state.Name = e.Name;
            state.Jar = new JarView();
            state.Participants = new List<ParticipantView>();

            return state;
        }

        public GroupView ApplyEvent(GroupView state, ParticipantAdded e)
        {
            state.Participants.Add(new ParticipantView { Id = e.ParticipantId, Name = e.Name });

            return state;
        }

        public GroupView ApplyEvent(GroupView state, ParticipantRemoved e)
        {
            state.Participants.Remove(state.Participants.Single(x => x.Id == e.ParticipantId));

            return state;
        }

        public GroupView ApplyEvent(GroupView state, ChargedForSwearWord e)
        {
            state.Participants.Single(x => x.Id == e.ParticipantId).Debt += e.Amount;

            return state;
        }

        public GroupView ApplyEvent(GroupView state, ChargedForSwearPhrase e)
        {
            state.Participants.Single(x => x.Id == e.ParticipantId).Debt += e.Amount;

            return state;
        }

        public GroupView ApplyEvent(GroupView state, DroppedIntoJar e)
        {
            state.Participants.Single(x => x.Id == e.ParticipantId).Debt -= e.Amount;
            state.Jar.Money += e.Amount;

            return state;
        }

        public GroupView ApplyEvent(GroupView state, TakenFromJar e)
        {
            state.Jar.Money -= e.Amount;

            return state;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace swear_jar.Projections
{
    public class ProjectionSource
    {
        public string StreamName { get; }

        public IReadOnlyList<object> Events { get; }

        public ProjectionSource(string streamName, IList<object> events)
        {

            if (streamName == null)
                throw new ArgumentNullException(nameof(streamName));

            if (string.IsNullOrEmpty(streamName))
                throw new ArgumentException(nameof(streamName));

            if (events == null)
                throw new ArgumentNullException(nameof(events));

            StreamName = streamName;
            Events = events.ToList().AsReadOnly();
        }
    }
}
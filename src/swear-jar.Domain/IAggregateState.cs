﻿using System;

namespace swear_jar.Domain
{
    public interface IAggregateState
    {
        Guid Id { get; set; }
        void Mutate(object e);
    }
}
﻿namespace swear_jar.Domain.Entities
{
    public class Participant : EntityBase
    {
        public string Name { get; set; }
        public int Debt { get; set; }

        public void Indebt(int amount)
        {
            Debt += amount;
        }

        public void Pay(int amount)
        {
            Debt -= amount;
        }
    }
}

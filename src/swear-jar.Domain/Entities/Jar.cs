﻿using System;

namespace swear_jar.Domain.Entities
{
    public class Jar : EntityBase
    {
        public Jar()
        {
            Id = Guid.NewGuid();
        }

        public int Money { get; set; }
    }
}
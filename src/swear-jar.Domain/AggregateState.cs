﻿using System;
using System.Collections.Generic;

namespace swear_jar.Domain
{
    public class AggregateState : IAggregateState
    {
        public Guid Id { get; set; }

        protected AggregateState() { }

        public AggregateState(IEnumerable<object> events)
        {
            foreach (var @event in events)
            {
                Mutate(@event);
            }
        }

        public void Mutate(object e)
        {
            ((dynamic) this).When((dynamic) e);
        }
    }
}
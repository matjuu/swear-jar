﻿using System;

namespace swear_jar.Domain
{
    public class EntityBase : IEntity
    {
        public Guid Id { get; set; }
    }
}

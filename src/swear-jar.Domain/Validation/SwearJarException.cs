﻿using System;

namespace swear_jar.Domain.Validation
{
    public class SwearJarException : Exception
    {
        public SwearJarException(string message) : base(message)
        {
        }
    }
}

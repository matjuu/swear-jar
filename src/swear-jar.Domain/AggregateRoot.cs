﻿using System.Collections.Generic;
using System.Linq;

namespace swear_jar.Domain
{
    public class AggregateRoot<TState> : IAggregateRoot where TState : IAggregateState
    {
        protected TState State { get; set; }

        private IList<object> _changes = new List<object>();

        public Identity Identity => new Identity(State.Id, GetType().Name);

        public object[] GetUncommittedEvents()
        {
            return _changes.ToArray();
        }

        public void ClearUncommittedEvents()
        {
            _changes.Clear();
        }
        public void Apply(object e)
        {
            State.Mutate(e);
            _changes.Add(e);
        }
    }
}
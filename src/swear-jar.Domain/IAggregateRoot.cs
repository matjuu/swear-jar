﻿namespace swear_jar.Domain
{
    public interface IAggregateRoot
    {
        void Apply(object e);
        object[] GetUncommittedEvents();
        void ClearUncommittedEvents();
    }
}
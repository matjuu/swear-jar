﻿using System;
using System.Collections.Generic;
using System.Linq;
using swear_jar.Domain.Contracts.Commands;
using swear_jar.Domain.Contracts.Events;
using swear_jar.Domain.Validation;

namespace swear_jar.Domain.Aggregates
{
    public class Group : AggregateRoot<GroupState>
    { 
        public Group(GroupState state)
        {
            State = state;
        }

        public Group(IEnumerable<object> events)
        { 
            State = new GroupState(events);
        }

        public void Create(CreateGroup command)
        {
            var @event = new GroupCreated
            {
                CorrelationId = command.CorrelationId,
                Id = command.Id,
                Name = command.Name
            };

            Apply(@event);
        }

        public void AddParticipant(AddParticipant command)
        {
            if (State.Participants.Any(x => x.Name == command.Name))
            {
                throw new SwearJarException("Participant name is already taken.");
            }

            var @event = new ParticipantAdded()
            {
                CorrelationId = command.CorrelationId,
                Id = command.GroupId,
                ParticipantId = Guid.NewGuid(),
                Name = command.Name
            };

            Apply(@event);
        }

        public void RemoveParticipant(RemoveParticipant command)
        {
            var participant = State.Participants.First(x => x.Id == command.ParticipantId);
            if (participant == null)
            {
                throw new SwearJarException("No such participant found in this group.");
            }

            var @event = new ParticipantRemoved()
            {
                CorrelationId = command.CorrelationId,
                Id = command.GroupId,
                ParticipantId = command.ParticipantId
            };

            Apply(@event);
        }

        public void TakeMoneyFromJar(TakeFromJar command)
        {
            if (State.Jar.Money < command.Amount)
            {
                throw new SwearJarException("The Jar doesn't contain enough money.");
            }

            var @event = new TakenFromJar()
            {
                CorrelationId = command.CorrelationId,
                Id = command.GroupId,
                Amount = command.Amount
            };

            Apply(@event);
        }

        public void ChargeForSwearing(MoneyTransactionCommand command)
        { 
            var @event = new ChargedForSwearWord()
            {
                CorrelationId = command.CorrelationId,
                Id = command.GroupId
            };

            Apply(@event);
        }

        public void ParticipantPays(DropIntoJar command)
        {
            var @event = new DroppedIntoJar()
            {
                CorrelationId = command.CorrelationId,
                Id = command.GroupId,
                Amount = command.Amount
            };

            Apply(@event);
        }
    }
}

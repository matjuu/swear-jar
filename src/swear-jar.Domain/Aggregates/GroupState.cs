using System.Collections.Generic;
using System.Linq;
using swear_jar.Domain.Contracts.Events;
using swear_jar.Domain.Entities;

namespace swear_jar.Domain.Aggregates
{
    public class GroupState : AggregateState
    {
        public string Name { get; set; }
        public List<Participant> Participants { get; set; } = new List<Participant>();
        public Jar Jar { get; private set; } = new Jar();

        public GroupState()
        { 
        }

        public GroupState(IEnumerable<object> events) : base(events)
        {
        }

        public void When(GroupCreated e)
        {
            Id = e.Id;
            Name = e.Name;
            Jar = new Jar();
            Participants = new List<Participant>();
        }

        public void When(ParticipantAdded e)
        {
            Participants.Add(new Participant {Id = e.ParticipantId, Name = e.Name});
        }

        public void When(ParticipantRemoved e)
        {
            Participants.Remove(Participants.Single(x => x.Id == e.ParticipantId));
        }

        public void When(ChargedForSwearWord e)
        {
            Participants.Single(x => x.Id == e.ParticipantId).Indebt(e.Amount);
        }

        public void When(ChargedForSwearPhrase e)
        {
            Participants.Single(x => x.Id == e.ParticipantId).Indebt(e.Amount);
        }

        public void When(DroppedIntoJar e)
        {
            Participants.Single(x => x.Id == e.ParticipantId).Pay(e.Amount);
            Jar.Money += e.Amount;
        }

        public void When(TakenFromJar e)
        {
            Jar.Money -= e.Amount;
        }
    }
}
﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using MyCouch;
using MyCouch.Requests;
using Newtonsoft.Json;
using swear_jar.Domain.Contracts.Views;
using swear_jar.Infrastructure.Validation;

namespace swear_jar.Infrastructure.Repositories
{
    public class GroupViewRepository : IViewRepository<GroupView>
    {   
        private readonly string _storeAddress = ConfigurationManager.AppSettings.Get("ProjectionStorage");

        public async Task<GroupView> GetAsync(Guid id)
        {
            using (var client = new MyCouchClient(_storeAddress, "swear-jar"))
            {
                var document = await client.Documents.GetAsync(id.ToString());

                if (document.IsSuccess == false)
                {
                    throw new RepositoryException("No such group exists.");
                }

                var view = JsonConvert.DeserializeObject<GroupView>(document.Content);
                return view;
            }
        }

        public async Task PersistAsync(GroupView view)
        {
            using (var client = new MyCouchClient(_storeAddress, "swear-jar"))
            {
                var document = await client.Documents.GetAsync(view.Id.ToString());
                var viewJson = JsonConvert.SerializeObject(view);

                if (document.IsSuccess)
                {
                    await
                        client.Documents.PutAsync(PutDocumentRequest.ForUpdate(view.Id.ToString(), document.Rev,
                            viewJson));
                }
                else
                {
                    await client.Documents.PutAsync(PutDocumentRequest.ForCreate(view.Id.ToString(), viewJson));
                }
                
            }
        }
    }
}

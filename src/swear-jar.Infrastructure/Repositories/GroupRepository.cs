﻿using System;
using System.Threading.Tasks;
using swear_jar.Domain;
using swear_jar.Domain.Aggregates;

namespace swear_jar.Infrastructure.Repositories
{
    public class GroupRepository : IRepository<Group>
    {
        private readonly IEventStore _eventStore;

        public GroupRepository(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public async Task<Group> Get(Guid id)
        {
            var identity = new Identity(id, typeof(Group).Name);
            var events = await _eventStore.GetEventStream(identity.ToString());
            var aggregate = new Group(events);

            return aggregate;
        }


        public async Task Save(Group aggregate)
        {
            var streamName = aggregate.Identity.ToString();
            await _eventStore.Save(streamName, aggregate);
        }
    }
}

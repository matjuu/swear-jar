﻿using System;
using System.Threading.Tasks;
using swear_jar.Domain.Aggregates;

namespace swear_jar.Infrastructure
{
    public interface IRepository<TEntity>
    {
        Task<TEntity> Get(Guid id);
        Task Save(Group aggregate);
    }
}
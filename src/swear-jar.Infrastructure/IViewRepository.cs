using System;
using System.Threading.Tasks;

namespace swear_jar.Infrastructure
{
    public interface IViewRepository<TView>
    {
        Task<TView> GetAsync(Guid id);
        Task PersistAsync(TView view);
    }
}
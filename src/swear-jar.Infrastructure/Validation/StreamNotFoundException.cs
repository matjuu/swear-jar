﻿using System;

namespace swear_jar.Infrastructure.Validation
{
    public class StreamNotFoundException : Exception
    {
        public StreamNotFoundException(string streamName) : base(streamName)
        {
            
        }
    }
}
﻿using System;

namespace swear_jar.Infrastructure.Validation
{
    public class RepositoryException : Exception
    {
        public RepositoryException(string message) : base(message)
        {
        }
    }
}
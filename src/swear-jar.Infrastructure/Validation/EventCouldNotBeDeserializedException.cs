﻿using System;

namespace swear_jar.Infrastructure.Validation
{
    public class EventCouldNotBeDeserializedException : Exception
    {
        public EventCouldNotBeDeserializedException(string eventClrTypeName) : base(eventClrTypeName)
        {
            
        }
    }
}
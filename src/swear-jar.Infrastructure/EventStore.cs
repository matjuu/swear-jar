﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using swear_jar.Domain;
using swear_jar.Domain.Contracts;
using swear_jar.Infrastructure.Validation;

namespace swear_jar.Infrastructure
{
    public class EventStore : IEventStore
    {
        private const string EVENT_CLR_TYPE_HEADER = "EventClrTypeName";

        private readonly IEventStoreConnection _eventStoreConnection;
        
        public EventStore()
        {
            _eventStoreConnection = EventStoreConnection.Create(
                 ConnectionSettings.Create()
                    .KeepReconnecting()
                    .SetHeartbeatTimeout(TimeSpan.FromSeconds(7))
                    .SetOperationTimeoutTo(TimeSpan.FromSeconds(30))
                    .SetDefaultUserCredentials(new UserCredentials("admin", "changeit")),
                new IPEndPoint(IPAddress.Parse("46.101.204.80"), 1113));

            _eventStoreConnection.ConnectAsync();
        }

        public async Task<IList<object>> GetEventStream(string streamName)
        {
            //TODO: Implement paging if by any case we need to read more events
            var eventStream = await
                _eventStoreConnection.ReadStreamEventsForwardAsync(streamName, 0, 4096, false);

            if (eventStream.Status == SliceReadStatus.StreamNotFound)
            {
                throw new StreamNotFoundException($"Stream {streamName} was not found.");
            }

            return eventStream.Events.Select(@event => DeserializeEvent(@event.OriginalEvent.Metadata, @event.OriginalEvent.Data)).ToList();
        }

        public async Task Save(string streamName, IAggregateRoot aggregate)
        {
            var events = aggregate.GetUncommittedEvents();

            if (!events.Any()) return;

            var expectedVersion = ExpectedVersion.Any;
            var eventsToSave = aggregate.GetUncommittedEvents().Select(x => ToEventData((Event) x));

            await _eventStoreConnection.AppendToStreamAsync(streamName, expectedVersion, eventsToSave);
        }

        private EventData ToEventData(Event e)
        {
            var typeName = e.GetType().Name;
            var eventData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(e));

            var eventHeaders = new Dictionary<string, string>
            {
                {EVENT_CLR_TYPE_HEADER, e.GetType().AssemblyQualifiedName}
            };

            var metadata = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(eventHeaders));

            return new EventData(e.EventId, typeName, true, eventData, metadata);
        }

        private object DeserializeEvent(byte[] metadata, byte[] data)
        {
            var meta = JsonConvert.DeserializeObject<IDictionary<string, string>>(Encoding.UTF8.GetString(metadata));

            var eventClrTypeName = meta[EVENT_CLR_TYPE_HEADER];
            
            var obj = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(data), Type.GetType(eventClrTypeName));

            if (obj.GetType() == typeof(JObject))
            {
                throw new EventCouldNotBeDeserializedException($"Could not deserialze to event of type {eventClrTypeName}");
            }

            return obj;
        }
    }

    public interface IEventStore
    {
        Task<IList<object>> GetEventStream(string streamName);
        Task Save(string streamName, IAggregateRoot aggregate);

    }
}

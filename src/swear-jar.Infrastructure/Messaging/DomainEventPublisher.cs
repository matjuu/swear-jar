﻿using System;
using System.Reflection;
using EasyNetQ;
using swear_jar.Domain;

namespace swear_jar.Infrastructure.Messaging
{
    public class DomainEventPublisher : IDomainEventPublisher
    {
        private readonly IBus _messageBus;

        public DomainEventPublisher(IBus messageBus)
        {
            _messageBus = messageBus;
        }

        public void Publish(dynamic @event)
        {
            _messageBus.Publish(@event);
        }

        public void Publish(IAggregateRoot aggregateRoot)
        {
            var events = aggregateRoot.GetUncommittedEvents();

            foreach (var @event in events)
            {
                _messageBus.Publish(DynamicCast(@event, @event.GetType()));
            }
        }

        private dynamic DynamicCast(object entity, Type to)
        {
            var openCast = typeof(DomainEventPublisher).GetMethod("Cast", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy | BindingFlags.DeclaredOnly);
            var closeCast = openCast.MakeGenericMethod(to);
            return closeCast.Invoke(entity, new[] { entity });
        }
        static T Cast<T>(object entity) where T : class
        {
            return entity as T;
        }
    }

    public interface IDomainEventPublisher
    {
        void Publish(dynamic @event);
        void Publish(IAggregateRoot aggregateRoot);
    }
}

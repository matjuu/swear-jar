﻿using System.Threading.Tasks;
using swear_jar.Domain;
using swear_jar.Domain.Contracts;

namespace swear_jar.ApplicationServices
{
    public interface IApplicationService<in TCommand> where TCommand : IMessage
    {
        Task Handle(TCommand command);
    }
}
﻿using System.Threading.Tasks;
using swear_jar.Domain.Contracts.Events;
using swear_jar.Domain.Contracts.Views;
using swear_jar.Infrastructure;
using swear_jar.Projections;

namespace swear_jar.ApplicationServices.Message_Handlers
{
    public class GroupViewEventHandler : ViewEventHandlerBase<GroupView>, IEventHandler<GroupCreated>, IEventHandler<ParticipantAdded>, IEventHandler<ParticipantRemoved>, IEventHandler<ChargedForSwearWord>, IEventHandler<ChargedForSwearPhrase>, IEventHandler<DroppedIntoJar>, IEventHandler<TakenFromJar>
    {
        public GroupViewEventHandler(IEventStore eventStore, IProjection<GroupView> projection, IViewRepository<GroupView> repository) : base(eventStore, projection, repository)
        {
        }

        public async Task HandleAsync(GroupCreated message)
        {
            await ProduceView($"Group-{message.Id}");
        }

        public async Task HandleAsync(ParticipantAdded message)
        {
            await ProduceView($"Group-{message.Id}");
        }

        public async Task HandleAsync(ParticipantRemoved message)
        {
            await ProduceView($"Group-{message.Id}");
        }

        public async Task HandleAsync(ChargedForSwearWord message)
        {
            await ProduceView($"Group-{message.Id}");
        }

        public async Task HandleAsync(ChargedForSwearPhrase message)
        {
            await ProduceView($"Group-{message.Id}");
        }

        public async Task HandleAsync(DroppedIntoJar message)
        {
            await ProduceView($"Group-{message.Id}");
        }

        public async Task HandleAsync(TakenFromJar message)
        {
            await ProduceView($"Group-{message.Id}");
        }
    }
}

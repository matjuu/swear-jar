﻿using System.Threading.Tasks;

namespace swear_jar.ApplicationServices.Message_Handlers
{
    public interface IEventHandler<TEvent>
    {
        Task HandleAsync(TEvent message);
    }
}
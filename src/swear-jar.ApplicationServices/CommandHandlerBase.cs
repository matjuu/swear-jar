﻿using System;
using System.Threading.Tasks;
using swear_jar.Domain;
using swear_jar.Domain.Aggregates;
using swear_jar.Domain.Contracts;
using swear_jar.Infrastructure;
using swear_jar.Infrastructure.Messaging;
using swear_jar.Infrastructure.Repositories;

namespace swear_jar.ApplicationServices
{
    public abstract class CommandHandlerBase<TCommand> : IApplicationService<TCommand> where TCommand : IMessage 
    {
        protected IRepository<Group> Repository { get; set; }
        protected IDomainEventPublisher DomainEventPublisher { get; set; }

        protected CommandHandlerBase(IRepository<Group> repository, IDomainEventPublisher domainEventPublisher)
        {
            Repository = repository;
            DomainEventPublisher = domainEventPublisher;
        }

        public virtual Task Handle(TCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

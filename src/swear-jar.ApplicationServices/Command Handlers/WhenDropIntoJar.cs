﻿using System.Threading.Tasks;
using swear_jar.Domain.Aggregates;
using swear_jar.Domain.Contracts.Commands;
using swear_jar.Infrastructure;
using swear_jar.Infrastructure.Messaging;
using swear_jar.Infrastructure.Repositories;

namespace swear_jar.ApplicationServices.Command_Handlers
{
    public class WhenDropIntoJar : CommandHandlerBase<DropIntoJar>
    {
        public WhenDropIntoJar(IRepository<Group> repository, IDomainEventPublisher publisher) : base(repository, publisher)
        {
        }

        public override async Task Handle(DropIntoJar command)
        {
            var aggregate = await Repository.Get(command.GroupId);

            aggregate.ParticipantPays(command);

            await Repository.Save(aggregate);
            DomainEventPublisher.Publish(aggregate);
        }
    }
}

﻿using System.Threading.Tasks;
using swear_jar.ApplicationServices.Validation;
using swear_jar.Domain.Aggregates;
using swear_jar.Domain.Contracts.Commands;
using swear_jar.Infrastructure;
using swear_jar.Infrastructure.Messaging;
using swear_jar.Infrastructure.Repositories;

namespace swear_jar.ApplicationServices.Command_Handlers
{
    public class WhenAddParticipant : CommandHandlerBase<AddParticipant>
    {
        public WhenAddParticipant(IRepository<Group> repository, IDomainEventPublisher publisher) : base(repository, publisher)
        {
        }

        public override async Task Handle(AddParticipant command)
        {
            if (string.IsNullOrEmpty(command.Name))
            {
                throw new CommandException("Participant must have a name.");
            }

            var aggregate = await Repository.Get(command.GroupId);

            aggregate.AddParticipant(command);

            await Repository.Save(aggregate);
            DomainEventPublisher.Publish(aggregate);
        }
    }
}

﻿using System.Threading.Tasks;
using swear_jar.Domain.Aggregates;
using swear_jar.Domain.Contracts.Commands;
using swear_jar.Infrastructure;
using swear_jar.Infrastructure.Messaging;
using swear_jar.Infrastructure.Repositories;

namespace swear_jar.ApplicationServices.Command_Handlers
{
    public class WhenRemoveParticipant : CommandHandlerBase<RemoveParticipant>
    {
        public WhenRemoveParticipant(IRepository<Group> repository, IDomainEventPublisher publisher) : base(repository, publisher)
        {
        }

        public override async Task Handle(RemoveParticipant command)
        {
            var aggregate = await Repository.Get(command.GroupId);

            aggregate.RemoveParticipant(command);

            await Repository.Save(aggregate);
            DomainEventPublisher.Publish(aggregate);
        }
    }
}

﻿using System.Threading.Tasks;
using swear_jar.ApplicationServices.Validation;
using swear_jar.Domain.Aggregates;
using swear_jar.Domain.Contracts.Commands;
using swear_jar.Infrastructure;
using swear_jar.Infrastructure.Messaging;
using swear_jar.Infrastructure.Repositories;

namespace swear_jar.ApplicationServices.Command_Handlers
{
    public class WhenCreateGroup : CommandHandlerBase<CreateGroup>
    {
        public WhenCreateGroup(IRepository<Group> repository, IDomainEventPublisher publisher) : base(repository, publisher)
        {
        }

        public override async Task Handle(CreateGroup command)
        {
            if (string.IsNullOrEmpty(command.Name))
            {
                throw new CommandException("Group must have a name");
            }

            var aggregate = new Group(new GroupState());
            aggregate.Create(command);

            await Repository.Save(aggregate);
            DomainEventPublisher.Publish(aggregate);
        }
    }
}

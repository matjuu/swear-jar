﻿using System;

namespace swear_jar.ApplicationServices.Validation
{
    public class CommandException : Exception
    {
        public CommandException(string message) : base(message)
        {
        }
    }
}

﻿using System.Threading.Tasks;
using swear_jar.Infrastructure;
using swear_jar.Projections;

namespace swear_jar.ApplicationServices
{
    public abstract class ViewEventHandlerBase<TView>
    {
        private readonly IViewRepository<TView> _repository;
        private readonly IEventStore _eventStore;
        private readonly IProjection<TView> _projection;

        protected ViewEventHandlerBase(IEventStore eventStore, IProjection<TView> projection, IViewRepository<TView> repository)
        {
            _eventStore = eventStore;
            _projection = projection;
            _repository = repository;
        }

        protected async Task ProduceView(string streamName)
        {
            var stream = await _eventStore.GetEventStream(streamName);
            var entity = await _projection.ProjectAsync(new ProjectionSource(streamName, stream));
            await _repository.PersistAsync(entity);
        }
    }
}
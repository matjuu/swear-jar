﻿using swear_jar.ApplicationServices.Message_Handlers;
using swear_jar.Domain.Contracts.Events;

namespace swear_jar.Configuration
{
    public class MessageBusConfiguration : MessageBusConfigurationBase
    {
        protected override void SubscribeEvents()
        {
            RegisterHandler<GroupCreated, GroupViewEventHandler>();
            RegisterHandler<ParticipantAdded, GroupViewEventHandler>();
            RegisterHandler<ParticipantRemoved, GroupViewEventHandler>();
            RegisterHandler<ChargedForSwearWord, GroupViewEventHandler>();
            RegisterHandler<ChargedForSwearPhrase, GroupViewEventHandler>();
            RegisterHandler<DroppedIntoJar, GroupViewEventHandler>();
            RegisterHandler<TakenFromJar, GroupViewEventHandler>();
        }
    }
}
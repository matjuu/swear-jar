﻿using System;
using EasyNetQ;
using Microsoft.Practices.Unity;
using swear_jar.ApplicationServices.Message_Handlers;

namespace swear_jar.Configuration
{
    public abstract class MessageBusConfigurationBase : IDisposable
    {
        private bool _disposeBus = false;

        public IBus Bus { get; private set; }
        private IUnityContainer Container { get; set; }

        protected MessageBusConfigurationBase()
        {
            CreateMessageBus();
        }

        protected MessageBusConfigurationBase(IBus bus)
        {
            Bus = bus;
        }

        private void CreateMessageBus()
        {
            _disposeBus = true;
            //TODO: Move this out of base class
            Bus = RabbitHutch.CreateBus("host=matjuu.space");
        }

        public void Dispose()
        {
            if (_disposeBus)
                Bus.Dispose();
        }

        public void Subscribe(IUnityContainer container)
        {
            Container = container;

            SubscribeEvents();
        }

        protected abstract void SubscribeEvents();

        protected void RegisterHandler<TMessage, THandler>() where TMessage : class where THandler : IEventHandler<TMessage>
        {
            //TODO: Resolve subscriptionId (by assembly maybe?)
            Bus.Subscribe<TMessage>("swear-jar", message =>
            {
                var handler = Container.Resolve<THandler>();
                handler.HandleAsync(message);
            });
        }
    }
}

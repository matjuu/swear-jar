﻿using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;

namespace swear_jar.Configuration
{
    public class UnityDependencyResolver : UnityScopeContainer, IDependencyResolver
    {
        public UnityDependencyResolver(IUnityContainer container)
            : base(container)
        {
        }

        public IDependencyScope BeginScope()
        {
            var childContainer = Container.CreateChildContainer();
            return new UnityScopeContainer(childContainer);
        }
    }
}

﻿using Microsoft.Practices.Unity;
using swear_jar.ApplicationServices;
using swear_jar.ApplicationServices.Command_Handlers;
using swear_jar.ApplicationServices.Message_Handlers;
using swear_jar.Domain.Aggregates;
using swear_jar.Domain.Contracts.Commands;
using swear_jar.Domain.Contracts.Views;
using swear_jar.Infrastructure;
using swear_jar.Infrastructure.Messaging;
using swear_jar.Infrastructure.Repositories;
using swear_jar.Projections;

namespace swear_jar.Configuration
{
    public class UnityContainerFactory
    {
        public static IUnityContainer CreateUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<IApplicationService<ChargeForSwearWord>, WhenChargeForSwearWord>();
            container.RegisterType<IApplicationService<ChargeForSwearPhrase>, WhenChargeForSwearPhrase>();
            container.RegisterType<IApplicationService<TakeFromJar>, WhenTakeFromJar>();
            container.RegisterType<IApplicationService<DropIntoJar>, WhenDropIntoJar>();
            container.RegisterType<IApplicationService<CreateGroup>, WhenCreateGroup>();
            container.RegisterType<IApplicationService<AddParticipant>, WhenAddParticipant>();
            container.RegisterType<IApplicationService<RemoveParticipant>, WhenRemoveParticipant>();

            container.RegisterType<ViewEventHandlerBase<GroupView>, GroupViewEventHandler>();

            container.RegisterType<IProjection<GroupView>, GroupProjection>();
            
            container.RegisterInstance<IEventStore>(new EventStore());
            container.RegisterType<IDomainEventPublisher, DomainEventPublisher>();

            container.RegisterType<IRepository<Group>, GroupRepository>();
            container.RegisterType<IViewRepository<GroupView>, GroupViewRepository>();

            return container;
        }
    }
}

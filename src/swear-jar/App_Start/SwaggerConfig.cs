using System.Web.Http;
using WebActivatorEx;
using swear_jar;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace swear_jar
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableSwagger(
                "swear-jar-help/{apiVersion}/docs",
                c =>
                {
                    c.RootUrl(req => "http://localhost:11420");
                    c.SingleApiVersion("v1", "Swear Jar API v1");
                    foreach (var comment in GetXmlCommentsPath())
                    {
                        c.IncludeXmlComments(comment);
                    }
                })
                .EnableSwaggerUi(
                "swear-jar-help/ui/{*assetPath}", c =>
                {
                    c.DisableValidator();
                    c.EnableDiscoveryUrlSelector();
                });
        }
        private static string[] GetXmlCommentsPath()
        {
            return new[]
            {
                "swear-jar.XML"
            };
        }
    }
}

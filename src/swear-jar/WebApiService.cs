﻿using System.Web.Http;
using System.Web.Http.SelfHost;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using swear_jar.Configuration;
using swear_jar.Controllers;
using swear_jar.Filters;

namespace swear_jar
{
    public class WebApiService
    {
        private readonly HttpSelfHostServer _server;
        private readonly HttpSelfHostConfiguration _config;

        private readonly IUnityContainer _unityContainer = UnityContainerFactory.CreateUnityContainer();
        private readonly MessageBusConfiguration _messageBusConfiguration;

        public WebApiService(string baseAddress)
        {
            _config = new HttpSelfHostConfiguration(baseAddress)
            {
                DependencyResolver = new UnityDependencyResolver(_unityContainer),
                IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly,
                
            };

            _messageBusConfiguration = new MessageBusConfiguration();
            _messageBusConfiguration.Subscribe(_unityContainer);
            _unityContainer.RegisterInstance(_messageBusConfiguration.Bus);

            RegisterControllers(_unityContainer);
            RegisterFilters();
            SwaggerConfig.Register(_config);
            ConfigureJsonSerializer();

            _config.MapHttpAttributeRoutes();

            _server = new HttpSelfHostServer(_config);
        }

        public void Start()
        {
            _server.OpenAsync();
        }

        public void Stop()
        {
            _server.CloseAsync();
            _server.Dispose();
            _messageBusConfiguration.Dispose();
        }

        private void RegisterControllers(IUnityContainer container)
        {
            container.RegisterType<JarController>();
            container.RegisterType<SwearController>();
            container.RegisterType<GroupController>();
        }

        private void RegisterFilters()
        {
            _config.Filters.Add(new ExceptionHandlerFilter());
        }

        private void ConfigureJsonSerializer()
        {
            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Include,
                    DefaultValueHandling = DefaultValueHandling.Include
                };

                return settings;
            };
        }
    }
}

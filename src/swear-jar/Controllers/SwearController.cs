﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Practices.Unity;
using swear_jar.ApplicationServices;
using swear_jar.Domain.Contracts.Commands;

namespace swear_jar.Controllers
{
    public class SwearController : ApiController
    {
        [Dependency]
        protected IApplicationService<ChargeForSwearWord> ChargeForSwearWordService { get; set; }
        [Dependency]
        protected IApplicationService<ChargeForSwearPhrase> ChargeForSwearPhraseService { get; set; }

        /// <summary>
        /// Charge a participant for saying a swear word.
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="participantGuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/swear/{groupGuid:guid}/{participantGuid:guid}/word")]
        public async Task<HttpResponseMessage> ChargeForSwearWord(Guid groupGuid, Guid participantGuid)
        {
            var command = new ChargeForSwearWord
            {
                CorrelationId = Guid.NewGuid(),
                GroupId = groupGuid,
                ParticipantId = participantGuid
            
            };
            await ChargeForSwearWordService.Handle(command);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }


        /// <summary>
        /// Charge a participant for saying a swear phrase.
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="participantGuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/swear/{groupGuid:int}/{participantGuid:int}/phrase")]
        public async Task<HttpResponseMessage> ChargeForSwearPhrase(Guid groupGuid, Guid participantGuid)
        {
            var command = new ChargeForSwearPhrase
            {
                CorrelationId = Guid.NewGuid(),
                GroupId = groupGuid,
                ParticipantId = participantGuid
            };

            await ChargeForSwearPhraseService.Handle(command);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}

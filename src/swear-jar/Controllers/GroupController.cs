﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Practices.Unity;
using swear_jar.ApplicationServices;
using swear_jar.Domain.Contracts.Commands;
using swear_jar.Domain.Contracts.Views;
using swear_jar.Infrastructure;

namespace swear_jar.Controllers
{
    public class GroupController : ApiController
    {
        [Dependency]
        protected IApplicationService<CreateGroup> CreateGroupService { get; set; }
        [Dependency]
        protected IApplicationService<AddParticipant> AddParticipantService { get; set; }
        [Dependency]
        protected IApplicationService<RemoveParticipant> RemoveParticipantService { get; set; }
        [Dependency]
        protected IViewRepository<GroupView> GroupViewStore { get; set; }


        /// <summary>
        /// Get group view.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/group/{id:guid}")]
        public async Task<GroupView> GetGroupByGuid(Guid id)
        {
            var group = await GroupViewStore.GetAsync(id);

            return group;
        }

        /// <summary>
        /// Create a new group with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/group")]
        public async Task<HttpResponseMessage> CreateGroup([FromBody]string name)
        {
            var command = new CreateGroup
            {
                CorrelationId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                Name = name
            };

            await CreateGroupService.Handle(command);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }


        /// <summary>
        /// Add a participant to a specified group.
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/group/{groupId:guid}/participant")]
        public async Task<HttpResponseMessage> AddParticipant(Guid groupId, [FromBody] string name)
        {
            var command = new AddParticipant
            {
                CorrelationId = Guid.NewGuid(),
                GroupId = groupId,
                Name = name
            };

            await AddParticipantService.Handle(command);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Remove a participant from a group.
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="participantId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/group/{groupId:guid}/participant")]
        public async Task<HttpResponseMessage> RemoveParticipant(Guid groupId, [FromBody] Guid participantId)
        {
            var command = new RemoveParticipant
            {
                CorrelationId = Guid.NewGuid(),
                GroupId = groupId,
                ParticipantId = participantId
            };
            await RemoveParticipantService.Handle(command);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}

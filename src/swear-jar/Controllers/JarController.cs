﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Practices.Unity;
using swear_jar.ApplicationServices;
using swear_jar.Domain.Contracts.Commands;

namespace swear_jar.Controllers
{
    public class JarController : ApiController
    {
        [Dependency]
        protected IApplicationService<DropIntoJar> DropIntoJarService { get; set; }
        [Dependency]
        protected IApplicationService<TakeFromJar> TakeFromJarService { get; set; }

        /// <summary>
        /// Participant drops specified amount of money into the groups jar.
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="participantGuid"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/jar/{groupGuid:guid}/{participantGuid:guid}/drop/{amount:int}")]
        public async Task<HttpResponseMessage> DropIntoJar(Guid groupGuid, Guid participantGuid, int amount)
        {
            var command = new DropIntoJar(amount)
            {
                CorrelationId = Guid.NewGuid(),
                GroupId = groupGuid,
                ParticipantId = participantGuid
            };
            await DropIntoJarService.Handle(command);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Take specified amount of money into the groups jar.
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/jar/{groupGuid:guid}/take/{amount:int}")]
        public async Task<HttpResponseMessage> TakeFromJar(Guid groupGuid, int amount)
        {
            var command = new TakeFromJar(amount)
            {
                CorrelationId = Guid.NewGuid(),
                GroupId = groupGuid,
            };

            await TakeFromJarService.Handle(command);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}

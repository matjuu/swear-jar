﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using swear_jar.ApplicationServices.Validation;
using swear_jar.Domain.Validation;
using swear_jar.Infrastructure.Validation;

namespace swear_jar.Filters
{
    public class ExceptionHandlerFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is SwearJarException)
            {
                var exception = (SwearJarException) context.Exception;
                var error = new Error {Message = exception.Message};

                context.Response = context.Request.CreateResponse(HttpStatusCode.NotFound, error);
            }
            else if (context.Exception is CommandException)
            {
                var exception = (CommandException) context.Exception;
                var error = new Error {Message = exception.Message};

                context.Response = context.Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
            else if (context.Exception is RepositoryException)
            {
                var exception = (RepositoryException) context.Exception;
                var error = new Error {Message = exception.Message};

                context.Response = context.Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
            else if (context.Exception is StreamNotFoundException)
            {
                var exception = (StreamNotFoundException) context.Exception;
                var error = new Error {Message = exception.Message};
            }
            else if (context.Exception is EventCouldNotBeDeserializedException)
            {
                var exception = (EventCouldNotBeDeserializedException) context.Exception;
                var error = new Error {Message = exception.Message};
            }
            else
            {
                var error = new Error {Message = "Internal server error."};

                context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }
    }
}
